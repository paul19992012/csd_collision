#Librerias
import config.database as db
import services.services as sv
import os
import services.yuval as yuval

#Funcion menu
def menu():
    print("**********************************************************")
    print("BÚSQUEDA DE COLISIONES: DOCUMENTOS ALTERNATIVOS")
    print("**********************************************************")
    print("1. Cargar inicial datos en la base de datos")
    print("2. Agregar un mensaje legítimo a la base de datos")
    print("3. Agregar un mensaje malicioso a la base de datos")
    print("4. Generar documentos de mensajes validos y erroneos")
    print("5. Buscar colisiones")
    print("6. Salir")
    opcion = input("Ingrese una opcion: ")
    return opcion


# Funcion principal del programa
def main():
    # Inicializacion de la base de datos
    db.Database.initialize()
    # Menu de opciones
    op = menu()
    while op != "6":
        if op == "1":
            sv.loadInitialData()
        elif op == "2":
            print("Agregando mensaje legítimo...")
        elif op == "3":
            print("Agregando mensaje malicioso...")
        elif op == "4":
            #Obtener los datos de la base de datos
            data_validate = db.Database.find("message_validate", {})
            #transformar los datos en una lista
            data_validate = list(data_validate)
            #Obtener el tamaño de los datos
            size_validate = len(data_validate)
            #Uso de la funcion generateDocuments con CRC32
            sv.generateDocuments(data_validate,size_validate,"CRC32",16,"valid")
            sv.generateDocuments(data_validate,size_validate,"CRC32",20,"valid")
            sv.generateDocuments(data_validate,size_validate,"CRC32",24,"valid")
            sv.generateDocuments(data_validate,size_validate,"CRC32",28,"valid")
            sv.generateDocuments(data_validate,size_validate,"CRC32",32,"valid")
            #Uso de la funcion generateDocuments con MD5
            sv.generateDocuments(data_validate,size_validate,"MD5",16,"valid")
            sv.generateDocuments(data_validate,size_validate,"MD5",20,"valid")
            sv.generateDocuments(data_validate,size_validate,"MD5",24,"valid")
            sv.generateDocuments(data_validate,size_validate,"MD5",28,"valid")
            sv.generateDocuments(data_validate,size_validate,"MD5",32,"valid")
            sv.generateDocuments(data_validate,size_validate,"MD5",36,"valid")
            sv.generateDocuments(data_validate,size_validate,"MD5",40,"valid")
            sv.generateDocuments(data_validate,size_validate,"MD5",44,"valid")
            
        elif op == "5":
            #Buscar colisiones
            yuval.findOneCollision("CRC32",16)
            yuval.findOneCollision("CRC32",20)
            yuval.findOneCollision("CRC32",24)
            yuval.findOneCollision("CRC32",28)
            yuval.findOneCollision("CRC32",32)
            yuval.findOneCollision("MD5",16)
            yuval.findOneCollision("MD5",20)
            yuval.findOneCollision("MD5",24)
            yuval.findOneCollision("MD5",28)
            yuval.findOneCollision("MD5",32)
            yuval.findOneCollision("MD5",36)
            yuval.findOneCollision("MD5",40)
            yuval.findOneCollision("MD5",44)
            
        else:
            print("Opcion no valida")
        op = menu()
        #esperar key para continuar
        input("Presione una tecla para continuar...")

# Ejecucion del programa
main() 

