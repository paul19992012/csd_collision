import pymongo
import os
from dotenv import load_dotenv
# Cargar las variables de entorno desde el archivo .env
load_dotenv()
class Database(object):
    URI = os.getenv("MONGODB_URI")
    DATABASE = os.getenv("DATABASE_NAME")

    @staticmethod
    def initialize():
        client = pymongo.MongoClient(Database.URI)
        Database.DATABASE = client[Database.DATABASE]

    @staticmethod
    def insert(collection, data):
        Database.DATABASE[collection].insert_one(data)

    @staticmethod
    def insert_many(collection, data):
        Database.DATABASE[collection].insert_many(data)

    @staticmethod
    def find(collection, query):
        return Database.DATABASE[collection].find(query)
    
    @staticmethod
    def find_one(collection, query):
        return Database.DATABASE[collection].find_one(query)
    
    @staticmethod
    def update(collection, query, data):
        Database.DATABASE[collection].update(query, data, upsert=True)

    @staticmethod
    def remove(collection, query):
        Database.DATABASE[collection].remove(query)

    @staticmethod
    def count(collection, query):
        return Database.DATABASE[collection].count_documents(query)
    
    @staticmethod
    def get_collection(collection):
        return Database.DATABASE[collection]
    
    @staticmethod
    def get_collections():
        return Database.DATABASE.collection_names()
    
    @staticmethod
    def drop_collection(collection):
        Database.DATABASE[collection].drop()
    
    @staticmethod
    def drop_collections():
        Database.DATABASE.collection.drop()
    
    @staticmethod
    def get_database():
        return Database.DATABASE
    
    @staticmethod
    def drop_database():
        Database.DATABASE.drop()
    
    @staticmethod
    def get_client():
        return Database.DATABASE.client
    
    @staticmethod
    def close_client():
        Database.DATABASE.client.close()