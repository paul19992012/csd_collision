
# Declaramos las librerias que vamos a utilizar
import json
import config.database as db
import os
import itertools
import time
import binascii
import hashlib
import psutil

# Obtener la ruta al directorio 'data' desde el directorio actual
ruta_data = os.path.join(os.path.dirname(__file__), '..', 'data')

# ******************************************************
# Funciones del Menu
# ******************************************************

# Funcion para cargar los datos iniciales en la base de datos
def loadInitialData():
    #Tiempo de ejecucion Inicial
    start_time = time.time()
    # Obtener información sobre el uso de memoria antes de llamar a la función
    memoria_inicial = psutil.virtual_memory().used
    # Obtener información sobre el uso de CPU antes de llamar a la función
    cpu_inicio = psutil.cpu_percent()
    
    #Eliminar las colecciones de la base de datos
    db.Database.drop_collection("message_validate")
    db.Database.drop_collection("message_error")
    
    print("Cargando datos en la base de datos...")

    # Cargar los datos en la base de datos de mensajes validos y erroneos
    datavalidate = readJSON(ruta_data + '/validatemessage.json')
    saveManyDataJson(datavalidate, "message_validate")

    datainvalidate = readJSON(ruta_data + '/errormessage.json')
    saveManyDataJson(datainvalidate, "message_error")

    print("Datos cargados en la base de datos")
     # Esperar un momento para permitir que psutil actualice las estadísticas de CPU
    time.sleep(1)
    
    elapsed_time = abs(time.time() - start_time)
    # Obtener información sobre el uso de memoria después de llamar a la función
    memoria_final = psutil.virtual_memory().used
    # Calcular la diferencia en el uso de memoria
    memoria_utilizada = abs(memoria_final - memoria_inicial)
   
    # Obtener información sobre el uso de CPU después de llamar a la función
    cpu_fin = psutil.cpu_percent()
    # Calcular la diferencia en el uso de CPU
    cpu_utilizado = abs(cpu_fin - cpu_inicio)
    
    print(f"Tiempo de ejecución: {elapsed_time:.2f} segundos")
    print(f"Uso de memoria RAM: {memoria_utilizada} bytes")
    print(f"Uso de CPU: {cpu_utilizado}%")
    
    #Almacenar datos de rendimiento en base de datos
    db.Database.insert("load_data", {
        "elapsed_time": elapsed_time,
        "memory_usage": memoria_utilizada,
        "cpu_usage": cpu_utilizado
    })



# Funcion para generar los documentos  de los mensajes validos y erroneos

def generateDocuments(data,size,hash_method,num_bits,type):
    #Tiempo de ejecucion Inicial
    start_time = time.time()
    # Obtener información sobre el uso de memoria antes de llamar a la función
    memoria_inicial = psutil.virtual_memory().used
    # Obtener información sobre el uso de CPU antes de llamar a la función
    cpu_inicio = psutil.cpu_percent()
    #Generar los documentos de los mensajes validos
    generateCombinationBinary(data, hash_method,num_bits,size, "message_hash",type)
    
     # Esperar un momento para permitir que psutil actualice las estadísticas de CPU
    time.sleep(1)
    
    elapsed_time = abs(time.time() - start_time)
    # Obtener información sobre el uso de memoria después de llamar a la función
    memoria_final = psutil.virtual_memory().used
    # Calcular la diferencia en el uso de memoria
    memoria_utilizada = abs(memoria_final - memoria_inicial)
   
    # Obtener información sobre el uso de CPU después de llamar a la función
    cpu_fin = psutil.cpu_percent()
    # Calcular la diferencia en el uso de CPU
    cpu_utilizado = abs(cpu_fin - cpu_inicio)
    
    print(f"Tiempo de ejecución Final: {elapsed_time:.2f} segundos")
    print(f"Uso de memoria RAM Final: {memoria_utilizada} bytes")
    print(f"Uso de CPU Final: {cpu_utilizado}%")
    
     # Almacenar datos de rendimiento en base de datos
    db.Database.insert("performance", {
        "hash_method": hash_method,
        "num_bits": num_bits,
        "elapsed_time": elapsed_time,
        "total_combinations": pow(2, size),
        "memory_usage_bytes": memoria_utilizada,
        "cpu_usage_percent": cpu_utilizado
    })


    




# ******************************************************
# Funciones privadas
# ******************************************************
# Funcion que lea el JSON y guardelo en la base de datos de mongodb
def readJSON(archivo):
    # Abre el archivo JSON Message Validate
    with open(archivo, 'r') as validate:
        return json.load(validate)
    
#Guardar un json en la base de datos
def saveManyDataJson(datos, namefile):
    # Inserta los datos en la base de datos
    db.Database.insert_many(namefile, datos)

def generateCombinationBinary(data, hash_method, num_bits, size, collection,type):
    start_time = time.time()
    total_combinations = pow(2, size)
    print("Generando documentos...")
    
    values_data_validate_A = [registro['A'] for registro in data]
    values_data_validate_B = [registro['B'] for registro in data]

    list_final = generateMessages(values_data_validate_A, values_data_validate_B,size,hash_method,num_bits,type,total_combinations,start_time)
    saveManyDataJson(list_final, collection+"_"+hash_method+"_"+str(num_bits))
    

# Funcion que transforma el mensaje generado en bits
def transformMessageBinary(message):
    # Codificar el texto en formato de bytes usando UTF-8
    bytes_texto = message.encode('utf-8')
    # Convierte los bytes en una secuencia de bits
    bits = ''.join(format(byte, '08b') for byte in bytes_texto)
    return bits

# Funcion que obtiene el hash crc32 del mensaje en bits
def getHashMessageBinaryCRC32(databits,num_bits):
   # Convierte el mensaje en bits en una cadena de bytes
    message_bytes = int(databits, 2).to_bytes((len(databits) + 7) // 8, byteorder='big')

    # Calcula el hash CRC32
    crc32_hash = binascii.crc32(message_bytes)
    
    # Convierte el valor de hash en una representación hexadecimal
    crc32_hex = format(crc32_hash & 0xFFFFFFFF, '08x')
    
    # Obtiene los primeros 5 caracteres del hash
    crc32_hex = crc32_hex[:num_bits//4]

    return crc32_hex

#Funcion que obtiene el hash md5 del mensaje en bits
def getHashMessageBinaryMD5(databits,num_bits):
    # Convierte el mensaje en bits en una cadena de bytes
    message_bytes = int(databits, 2).to_bytes((len(databits) + 7) // 8, byteorder='big')

    # Calcula el hash MD5
    md5_hash = hashlib.md5(message_bytes)

    # Convierte el valor de hash en una representación hexadecimal
    md5_hex = md5_hash.hexdigest()[:num_bits//4]

    return md5_hex

def getHashMethod(hash_method):
    if hash_method == "CRC32":
        return getHashMessageBinaryCRC32
    elif hash_method == "MD5":
        return getHashMessageBinaryMD5
    else:
        return getHashMessageBinaryCRC32
    
def save_data(data_json, hash_combinacion, message, type_message, index, total_combinations, start_time):
    data_json.append({
        "hash": hash_combinacion,
        "message": message,
        "type": type_message
    })
    progress = (index + 1) / total_combinations * 100
    print(f"Progreso: {progress:.2f}%")
    elapsed_time = time.time() - start_time
    print(f"Tiempo de ejecución: {elapsed_time:.2f} segundos")
    #calcular el tiempo restante
    time_remaining = elapsed_time * (total_combinations / (index + 1) - 1)
    print(f"Tiempo restante: {time_remaining:.2f} segundos")
    #limpiar la pantalla
    os.system('cls')
    
    return data_json

# Funcion que permite generar las combinaciones de los mensajes validos y erroneos
def generateMessages(datosA, datosB, size, hash_method, num_bits, type, total_combinations, start_time):
    datos_json = []
    getHashMessageBinary = getHashMethod(hash_method)

    for index, combinacion in enumerate(itertools.product([0, 1], repeat=size)):
        resultado = ''.join([datosB[i] if valor == 1 else datosA[i] for i, valor in enumerate(combinacion)])

        if index % 10000 == 0:
            # Imprimir el progreso cada 10000 iteraciones
            progress = (index + 1) / total_combinations * 100
            print(f"Progreso: {progress:.2f}%")

        hash_combinacion = getHashMessageBinary(transformMessageBinary(resultado), num_bits)

        # Guardar datos solo una vez al final del bucle
        datos_json.append({
            "hash": hash_combinacion,
            "message": resultado,
            "type": type
        })

    elapsed_time = time.time() - start_time
    print(f"Tiempo de ejecución: {elapsed_time:.2f} segundos")
    return datos_json

def generateBadMessage(data, combinacion, hash_method, num_bits):
    datosA = [registro['A'] for registro in data]
    datosB = [registro['B'] for registro in data]
    getHashMessageBinary = getHashMethod(hash_method)
    resultado = ''.join([datosB[i] if valor == 1 else datosA[i] for i, valor in enumerate(combinacion)])
    hash_combinacion = getHashMessageBinary(transformMessageBinary(resultado), num_bits)
    return {
        "hash": hash_combinacion,
        "message": resultado,
    }
