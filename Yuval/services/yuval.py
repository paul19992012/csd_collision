
# Declaramos las librerias que vamos a utilizar
import itertools
import time
import psutil
import config.database as db
import services.services as sv

# Declaracion de variables globales

dataValidate =  []
errorValidate = []

# Funcion que permite encontrar una colision
def findOneCollision(hashmethod,num_bits):
    #Tiempo de ejecucion Inicial
    start_time = time.time()
    # Obtener información sobre el uso de memoria antes de llamar a la función
    memoria_inicial = psutil.virtual_memory().used
    # Obtener información sobre el uso de CPU antes de llamar a la función
    cpu_inicio = psutil.cpu_percent()
    
    # Almacenar colisiones
    colisiones = []
    errores = []
    
    # Leer los datos de la colección message_error
    #Obtener los datos de la base de datos
    data_error = db.Database.find("message_error", {})
    #transformar los datos en una lista
    data_error = list(data_error)
    #Obtener el tamaño de los datos
    size_error = len(data_error)
    collection_name = "message_hash_"+hashmethod+"_"+str(num_bits)
    
    for index, combinacion in enumerate(itertools.product([0, 1], repeat=size_error)):
        messageBad = sv.generateBadMessage(data_error,combinacion,hashmethod,num_bits)
        #Buscar el mensaje malicioso en la base de datos
        data = db.Database.find_one(collection_name, {
            "hash": messageBad["hash"]
        })
        if(data):
            colisiones.append({
                "hash": data["hash"],
                "message_valid": data["message"],
                "message_error": messageBad["message"],
                "index": index
            })
            break
        else:
            errores.append({
                "message_error": messageBad["message"],
                "hash": messageBad["hash"],
                "index": index
            })
            
    # Si no hay colisiones
    if not colisiones:
        print("No se encontraron colisiones")
    else:
        # Detalle de Colision
        print("Colision encontrada")
        print(colisiones)
 
     # Esperar un momento para permitir que psutil actualice las estadísticas de CPU
    time.sleep(1)
    
    elapsed_time = abs(time.time() - start_time)
    # Obtener información sobre el uso de memoria después de llamar a la función
    memoria_final = psutil.virtual_memory().used
    # Calcular la diferencia en el uso de memoria
    memoria_utilizada = abs(memoria_final - memoria_inicial)
   
    # Obtener información sobre el uso de CPU después de llamar a la función
    cpu_fin = psutil.cpu_percent()
    # Calcular la diferencia en el uso de CPU
    cpu_utilizado = abs(cpu_fin - cpu_inicio)
    
    print(f"Tiempo de ejecución Final: {elapsed_time:.2f} segundos")
    print(f"Uso de memoria RAM Final: {memoria_utilizada} bytes")
    print(f"Uso de CPU Final: {cpu_utilizado}%")
    
     # Almacenar datos de rendimiento en base de datos de la colision
    db.Database.insert("results", {
        "hash_method": hashmethod,
        "num_bits": num_bits,
        "elapsed_time": elapsed_time,
        "memory_usage_bytes": memoria_utilizada,
        "cpu_usage_percent": cpu_utilizado,
        "collisions": colisiones,
        "errors": errores
    })